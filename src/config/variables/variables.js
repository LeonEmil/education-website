const courses = [
    {
        "id": 1,
        "title": "React desde cero",
        "image": "https://drupal.ed.team/sites/default/files/styles/16_9_medium/public/imagenes-cdn-edteam/2019-04/React%20desde%20cero%20%281%29.png",
        "price": 30,
        "professor": "Beto Quiroga",
    },
    {
        "id": 2,
        "title": "Fundamentos de redes",
        "image": "https://drupal.ed.team/sites/default/files/imagenes-cdn-edteam/2019-03/Redes%20Fundamentos.png",
        "price": 20,
        "professor": "Paula Leon",
    },
    {
        "id": 3,
        "title": "Java desde cero",
        "image": "https://drupal.ed.team/sites/default/files/styles/medium/public/courses/images/java%20desde%20cero.jpg",
        "price": 30,
        "professor": "Beto Quiroga",
    },
    {
        "id": 4,
        "title": "Animaciones en CSS",
        "image": "https://drupal.ed.team/sites/default/files/styles/medium/public/courses/images/css-animaciones-poster-1280.jpg?itok=RVKU2XUo",
        "price": 25,
        "professor": "Alvaro Felipe",
    },
]

export default courses