import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const CourseCard = ({title, image, price, professor, id}) => (
        <article class="card">
                <div class="img-container s-ratio-16-9 s-radius-tr s-radius-tl">
                        <Link to={`/cursos/${id}`}>
                                <img src={image} alt={`Portada de ${title}`} />
                        </Link>
                </div>
                        <div class="card__data s-border s-radius-br s-radius-bl s-pxy-2">
                                <h3 class="t5 s-mb-2 s-center">
                                        {title}
                                </h3>
                                <div class="s-mb-2 s-main-center">
                                        <div class="card__teacher s-cross-center">
                                                <div class="card__avatar s-mr-1">
                                                        <div class="circle img-container">
                                                                <img src="path/img" alt="Desc" />
                                                        </div>
                                                </div>
                                        <span class="small">{professor}</span>
                                        </div>
                                </div>
                                <div class="s-main-center">
                                        <a class="button--ghost-alert button--tiny" href="ed.team">${price}USD</a>
                                </div>
                        </div>
        </article>
)

CourseCard.propTypes = {
        title: PropTypes.string,
        image: PropTypes.string,
        price: PropTypes.number,
        profesor: PropTypes.string
}

CourseCard.defaultProps = {
        title: "No se encontró título",
        image: "http://www.ciudaddelapunta.com/sitio/fotos/ciudad/miniaturas/006.jpg",
        price: "--",
        profesor: ""
}


export default CourseCard