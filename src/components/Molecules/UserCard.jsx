import React from "react"

const UserCard = ({name, username, email}) => {

    return(
        <article classNameName="card">
                <div className="card__data s-border s-radius-br s-radius-bl s-pxy-2">
                    <h3 className="t5 s-mb-2 s-center">
                        { name }
                    </h3>
                    <div className="s-mb-2 s-main-center">
                        <div className="card__teacher s-cross-center">
                            <div className="card__avatar s-mr-1">
                                <div className="circle img-container">
                                <img src="http://covitalidad.edu.umh.es/wp-content/uploads/sites/1352/2018/06/default-user.png" alt="Foto de usuario" />
                            </div>
                                </div>
                                <span className="small"> { username } </span>
                            </div>
                        </div>
                        <div className="s-main-center">
                            <a className="button--ghost-alert button--tiny" href="com.com"> { email } </a>
                        </div>
                    </div>
        </article>
    )

}

export default UserCard