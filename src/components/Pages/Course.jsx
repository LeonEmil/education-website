import React from 'react'
import NotFound from './404'
import courses from '../../config/variables/variables'

const Course = ({match}) => {

    const currentCourse = courses.filter(course => course.id === parseInt(match.params.id))[0]

    return(
        currentCourse ?
            <div className="ed-grid m-grid-3">
                <h1 className="m-cols-3">{currentCourse.title}</h1>
                <img className="m-cols-1" src={currentCourse.image} alt={`Poster de ${currentCourse.title}`}/>
                <p className="m-cols-2">{currentCourse.professor}</p>
            </div>
        : <NotFound />
        
    )
}

export default Course