import React from "react"
import UserCard from '../Molecules/UserCard'

class Users extends React.Component{

    constructor(props){
        super(props)

        this.state = {
            users: [],
        }
    }

    componentDidMount() {
        fetch('https://jsonplaceholder.typicode.com/users', { method: 'get' })
            .then(response => response.json())
            .then(response2 => {
                this.setState({
                    users: response2
                })
            })
    }

    render() {

        const users = this.state.users

        return(
            <div className="ed-grid">
                <h1>Hola</h1>
                <div className="ed-grid m-grid-3 l-grid-4">
                    {
                        users.map(user => 
                            <UserCard 
                                key={user.id} 
                                name={user.name} 
                                username={user.username}
                                email={user.email} 
                            />
                        )
                    }
                </div>
            </div>
        )
    }
}

export default Users