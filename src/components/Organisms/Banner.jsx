import React from 'react'

const Banner = () => (
    <div className="main-banner img-container s-mb-4" id="main-banner">
        <div className="ed-grid lg-grid-6">
            <div className="lg-cols-4 lg-x-2">
                <img className="main-banner__img" src="https://i.pinimg.com/originals/2b/84/53/2b8453d9c9f6e1e1c35dcb523e516478.jpg" alt="Descripcion"/>
                <div className="main-banner__data s-center">
                    <p className="t2 s-mb-0">Título del banner</p>
                    <p> Subtítulo del banner</p>
                    <a href="http://w3c.org" className="button">Botón del banner</a>
                </div>
            </div>
        </div>
    </div>
)

export default Banner