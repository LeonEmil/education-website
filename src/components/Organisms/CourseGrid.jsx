import React from 'react'
import CourseCard from '../Molecules/CourseCard'
import courses from '../../config/variables/variables'
import WithLoader from '../HOC/withLoader'

const CourseGrid = () => (
    <div className="grid-container ed-grid m-grid-4">
        { courses.map( curso => ( 
            <CourseCard 
                title={curso.title}
                profesor={curso.professor}
                image={curso.image}
                price={curso.price}
                id={curso.id}
                key={curso.id}

            /> 
            ) ) }
    </div>
)

export default WithLoader(CourseGrid)