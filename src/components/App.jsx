import React from 'react';
import '../sass/style.scss'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import CourseGrid from './Organisms/CourseGrid'
import Formulario from './Pages/Form';
import Course from './Pages/Course'
import NotFound from './Pages/404';
import MainMenu from './Organisms/MainMenu';
import Home from './Pages/Home';
import Users from './Pages/Users';
// import Cursos from './components/cursos'
// import Formulario from './components/formulario';

function App() {
  return (
    <Router>
      <MainMenu />
      <Switch>
        <Route path="/" exact component={ Home } />
        <Route path="/cursos/:id" component={ Course } />
        <Route path="/cursos" component={ CourseGrid } />
        <Route path="/formulario" component={ () => <Formulario nombre="Contacto" /> } />
        <Route path="/usuarios" component={ Users } />
        <Route component={ NotFound } />
      </Switch>
    </Router>
  )
}

export default App;
