import React from "react"

const WithLoader = (WrappedComponent) => {
    return class WithLoader extends React.Component {

        constructor(props){
            super(props)
        }

        render(){
            return <WrappedComponent {...this.props} />
        }
    }
}

export default WithLoader